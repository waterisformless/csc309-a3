<LINK href="<?php echo base_url(); ?>/css/list.css" rel="stylesheet" type="text/css">
<h2 class="admin">Admin Page</h2>
<h2 class="user">Product Table</h2>
<div id="badproduct" class="ui small modal">
    <?php
    // hide admin controls for non-admins
    if ($this->session->userdata('logged_in')!='admin'){
        echo "<style>.admin, .admin{display:none;}</style>";
    }
    if ($this->session->userdata('logged_in')=='admin'){
        echo "<style>.user, .user{display:none;}</style>";
    }

    echo "<form action='" . site_url('store/checkout') . "'>";
    echo "<button class='user' type='submit'>View Cart</button>";
    echo "</form>";

    echo "<form action='" . site_url('store/newForm') . "'>";
    echo "<button class='admin' type='submit'>Add New Product</button>";
    echo "</form>";
    
    echo "<form action='" . site_url('store/displayOrders') . "'>";
    echo "<button class='admin' type='submit'>Display Orders</button>";
    echo "</form>";
    
    echo "<form action='" . site_url('store/deleteUsers') . "'>";
    echo "<button class='admin' type='submit'>Delete All Users</button>";
    echo "</form>";
    
    echo "<form action='" . site_url('store/logout') . "'>";
    echo "<button type='submit'>Logout</button>";
    echo "</form>";
    
    
    echo "<table>";
    echo "<tr><th>Name</th><th>Description</th><th>Price</th><th>Photo</th></tr>";

    foreach ($products as $product) {
        echo "<tr>";
        echo "<td>" . $product->name . "</td>";
        echo "<td>" . $product->description . "</td>";
        echo "<td>$" . $product->price . "</td>";
        echo "<td><img src='" . base_url() . "images/product/" . $product->photo_url . "' width='100px' /></td>";

        echo "<td class='admin'>" . anchor("store/delete/$product->id", 'Delete',
                "onClick='return confirm(\"Do you really want to delete this record?\");'") . "</td>";
        echo "<td class='admin'>" . anchor("store/editForm/$product->id", 'Edit') . "</td>";
        echo "<td>" . anchor("store/read/$product->id", 'View') . "</td>";
        echo "<td class='user'>" . anchor("store/addToCart/$product->id", 'Add to Cart') . "</td>";
        echo "</tr>";
    }
    echo "</table>";

    ?>
