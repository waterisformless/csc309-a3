<LINK href="<?php echo base_url(); ?>/css/list.css" rel="stylesheet" type="text/css">
<h2>Checkout</h2>
<div id="badproduct" class="ui small modal">
<?php
echo "<div class='checkout'><table>";
echo "<tr><th>Name</th><th>Description</th><th>Price</th><th>Photo</th><th>Quantity</th><th></th><th>Total Price</th></tr>";

$order = array();

foreach ($cart as $item) {
    foreach ($products as $product) {
        if ($product->id == $item[0]) {
            echo "<tr>";
            echo "<td>" . $product->name . "</td>";
            echo "<td>" . $product->description . "</td>";
            echo "<td>$" . $product->price . "</td>";
            echo "<td><img src='" . base_url() . "images/product/" . $product->photo_url . "' width='100px' />
					    </td>";
            echo "<td>" . ltrim(strval($item[1]), "0'");
            echo form_open_multipart('store/editQuantity/');
            echo form_hidden('id', $item[0]);
            echo form_error('quantity');
            echo "<input type='number' name='quantity' value = '" . $item[1] . "' min='1' placeholder='" . $item[1] . "'required />";
            echo form_submit('submit', 'Update');            
         
            echo form_close();
            
            echo "</td>";
            echo "<td>" . anchor("store/removeFromCart/$item[0]", 'Delete') . "</td>";
            echo "<td>$" . $product->price * $item[1] . "</td>";
            echo "</tr>";

            $order[] = array($product->id, $item[1]);
        }
    }
}
echo "</table>";
echo "<br/><section id=checkout>Grand Total: $" . $this->session->userdata('total') . "</section>";
echo "</div><br/>";

echo form_open_multipart('store/submitOrder');

echo "<span>Please enter your credit card information: </span><br/>";

echo form_label('Credit Card Number');
echo form_error('creditcard_number');
echo '<input type="number" name="creditcard_number" value required>';
echo "<br/>";
echo form_label('Expiry Month');
echo form_error('creditcard_month');
echo '<input type="number" name="creditcard_month" value required min="1" max="12">';
echo "<br/>";
echo form_label('Expiry Year');
echo form_error('creditcard_year');
echo '<input type="number" name="creditcard_year" value required>';

echo form_submit('submit', 'Confirm Checkout');
echo form_close();

echo "<form action='" . site_url('store') . "'>";
echo "<button class='user' type='submit'>Back to Store</button>";
echo "</form>";
?>
