<style>body{font-family: "Courier New", Courier, monospace;}table tr{text-align: center;}table th, table td{border:1px solid;}</style>
<?php

$content = "";
$email = $user->email;

$content .= $user->first . " " . $user->last;
$content .= "</br>";
$content .= $user->email;
$content .= "</br>";
$content .= "Order#: " . $order_id;
$content .= "</br>";

$content .= "<table>";
$content .= "<tr><th>Name</th><th>Description</th><th>Price</th><th>Photo</th><th>Quantity</th><th>Total Price</th></tr>";

foreach ($products as $product) {
	foreach ($order_items as $item) {
		if ($item->order_id == $order_id) {
			if ($product->id == $item->product_id) {
				$content .= "<tr>";
				$content .= "<td>" . $product->name . "</td>";
				$content .= "<td>" . $product->description . "</td>";
				$content .= "<td>" . $product->price . "</td>";
				$content .= "<td><img src='" . base_url() . "images/product/" . $product->photo_url . "' width='100px' />
				</td>";
				$content .= "<td>" . ltrim(strval($item->quantity), "0'") . "</td>";
				$content .= "<td>$" . $product->price * $item->quantity . "</td>";				
				$content .= "</tr>";
			}
		}
	}
}
$content .= "<tr><td></td><td></td><td></td><td></td><td>Total:</td><td>$" . $order->row()->total . "</td>";
$content .= "</table>";

$eparam = array($content, $email);
$this->session->set_userdata('eparam', $eparam);

echo $content;
echo "<button onclick='window.print();'>Print!</button>";
echo "<br>";
echo "<form action='" . site_url('/email/') . "'>";
echo "<button class='user' type='submit'>Email</button>";
echo "</form>";
echo "<p>" . anchor('store/index', 'Back') . "</p>";
?>