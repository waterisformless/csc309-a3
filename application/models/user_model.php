<?php

/**
 * Created by PhpStorm.
 * User: Li
 * Date: 2014-11-13
 * Time: 5:27 PM
 */
class User_model extends CI_Model
{
    function login($login, $password)
    {
        $query = $this->db->query('SELECT login FROM customers WHERE login = "' . $login . '" AND password = "' . $password . '"');

        return $query;
    }

    function get($login)
    {
        $query = $this->db->query('SELECT id, login, password, first, last, email FROM customers WHERE login = "' . $login . '"');
    	      	
    	return $query;
    }
    
    function getByID($id)
    {
    	$query = $this->db->query('SELECT id, login, password, first, last, email FROM customers WHERE id = "' . $id . '"');
    
    	return $query;
    }
    
    function insert($user)
    {
        return $this->db->insert("customers", array('first' => $user->first,
            'last' => $user->last,
            'login' => $user->login,
            'password' => $user->password,
            'email' => $user->email));
    }

    function deleteAll()
    {
        if ($this->session->userdata('logged_in') == 'admin') {
            $this->db->query("DELETE FROM customers WHERE login <> 'admin'");
            return True;
        }
        return False;
    }
}