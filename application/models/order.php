<?php

/**
 * Created by PhpStorm.
 * User: Li
 * Date: 2014-11-14
 * Time: 2:16 PM
 */
class Order extends CI_Model
{

    function submit($customer, $total, $cc_number, $cc_month, $cc_year)
    {
        $data = array(
            'customer_id' => $customer,
            'order_date'=> date("Y-m-d"),
            'order_time'=> date("h:i:s"),
            'total' => $total,
            'creditcard_number' => $cc_number,
            'creditcard_month' => $cc_month,
            'creditcard_year' => $cc_year
        );
        $this->db->insert('orders', $data);
        $order_id = $this->db->insert_id();
        return $order_id;
    }
    
    function get($id)
    {
    	$query = $this->db->get_where('orders', array('id' => $id));
    
    	return $query;
    }    

    function insert_order_item($order_id, $product, $quantity)
    {
        $data = array(
            'order_id' => $order_id,
            'product_id' => $product,
            'quantity'=> $quantity
        );
        $this->db->insert('order_items', $data);
    }

    function get_order_items()
    {  	 
    	$query = $this->db->get('order_items');
    	
        return $query->result('OrderItem');
	}
    
    function deleteAll()
    {
        if ($this->session->userdata('logged_in') == 'admin') {
            $this->db->query("DELETE FROM orders");
            return True;
        }
        return False;
    }

    function displayAll()
    {
        $query = $this->db->get('orders');
        return $query->result('Order_Object');
    }
} 