<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Verify_Login extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('User');
        $this->load->model('User_model');
    }

    function index()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('login', 'Login', 'required|trim|max_length[16]');
        $this->form_validation->set_rules('password', 'Password', 'required|max_length[16]|callback_check_password');
        //$this->form_validation->set_rules('email', 'Email','required|trim|max_length[45]|valid_email');

        if ($this->form_validation->run() == FALSE) {
            //Field validation failed.  Refresh login page
            $this->load->view('login_view');
        } else {
            //Go to private area
            redirect('store', 'refresh');
        }

    }
    
    function check_password($password)
    {
    	//Field validation succeeded.  Validate against database
    	$login = $this->input->post('login');
    
    	//check if password matches password in db
    	$result = $this->User_model->login($login, $password);
    
    	//login the user or set validation error
    	if ($result->num_rows() > 0) {
    		$logged_in_user = $result->row(0, 'User');
    		if ($logged_in_user->login != $this->session->userdata('logged_in')){
    			//remove all items from cart if different user
    			$this->session->set_userdata('cart', array());
    		}
    		if (session_status() === PHP_SESSION_NONE){
    			session_start();
    		}
    		$this->session->set_userdata('logged_in', $logged_in_user->login);
    	} else {
    		$this->form_validation->set_message('check_password', 'Invalid username or password');
    		return false;
    	}
    }
}

?>