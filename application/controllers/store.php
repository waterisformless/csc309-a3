<?php

class Store extends CI_Controller
{


    function __construct()
    {
        // Call the Controller constructor
        parent::__construct();


        $config['upload_path'] = './images/product/';
        $config['allowed_types'] = 'gif|jpg|png';
        /*	    	$config['max_size'] = '100';
                    $config['max_width'] = '1024';
                    $config['max_height'] = '768';
        */

        $this->load->library('upload', $config);

    }

    function index()
    {
		if ($this->session->userdata('logged_in')){
	        $this->load->model('product_model');
	        $products = $this->product_model->getAll();
	        $data['products'] = $products;
	        $this->load->view('product/list.php', $data);
	
	        // reset any session errors
	        if (isset($_SESSION['errno'])) {
	            $data['errmsg'] = $_SESSION['errmsg'];
	            $data['errno'] = $_SESSION['errno'];
	            unset($_SESSION['errno']);
	            unset($_SESSION['errmsg']);
	        }
		} else {
            redirect('login', 'refresh');
		}
    }

    function newForm()
    {
    	if ($this->session->userdata('logged_in')){
	        if ($this->session->userdata('logged_in')=='admin') {
	            $this->load->view('product/newForm.php');
	        }
	        else{
	            echo "<script>alert('BAD BOY! You are not admin!');</script>";
	            redirect('store', 'refresh');
	        }
        } else {
        	redirect('login', 'refresh');
        }
    }

    function create()
    {
    	if ($this->session->userdata('logged_in')){
	        $this->load->library('form_validation');
	        $this->form_validation->set_rules('name', 'Name', 'required|is_unique[products.name]');
	        $this->form_validation->set_rules('description', 'Description', 'required');
	        $this->form_validation->set_rules('price', 'Price', 'required');
	
	        $fileUploadSuccess = $this->upload->do_upload();
	
	        if ($this->form_validation->run() == true && $fileUploadSuccess) {
	            $this->load->model('product_model');
	
	            $product = new Product();
	            $product->name = $this->input->get_post('name');
	            $product->description = $this->input->get_post('description');
	            $product->price = $this->input->get_post('price');
	
	            $data = $this->upload->data();
	            $product->photo_url = $data['file_name'];
	
	            $this->product_model->insert($product);
	
	            //Then we redirect to the index page again
	            redirect('store/index', 'refresh');
	        } else {
	            if (!$fileUploadSuccess) {
	                $data['fileerror'] = $this->upload->display_errors();
	                $this->load->view('product/newForm.php', $data);
	                return;
	            }
	
	            $this->load->view('product/newForm.php');
	        }
        } else {
        	redirect('login', 'refresh');
        }
    }

    function read($id)
    {
        if ($this->session->userdata('logged_in')){
	    	$this->load->model('product_model');
	        $product = $this->product_model->get($id);
	        $data['product'] = $product;
	        $this->load->view('product/read.php', $data);
        } else {
        	redirect('login', 'refresh');
        }
    }

    function editForm($id)
    {
    	if ($this->session->userdata('logged_in')){
	    	if ($this->session->userdata('logged_in')=='admin') {
	            $this->load->model('product_model');
	            $product = $this->product_model->get($id);
	            $data['product'] = $product;
	            $this->load->view('product/editForm.php', $data);
	        }
	        else{
	            echo "<script>alert('BAD BOY! You are not admin!');</script>";
	            show_error("403: Read access error", 403);
	        }
        } else {
        	redirect('login', 'refresh');
        }
    }

    function update($id)
    {
    	if ($this->session->userdata('logged_in')){	    		 
	        $this->load->library('form_validation');
	        $this->form_validation->set_rules('name', 'Name', 'required');
	        $this->form_validation->set_rules('description', 'Description', 'required');
	        $this->form_validation->set_rules('price', 'Price', 'required');
	
	        if ($this->form_validation->run() == true) {
	            $product = new Product();
	            $product->id = $id;
	            $product->name = $this->input->get_post('name');
	            $product->description = $this->input->get_post('description');
	            $product->price = $this->input->get_post('price');
	
	            $this->load->model('product_model');
	            $this->product_model->update($product);
	            //Then we redirect to the index page again
	            redirect('store/index', 'refresh');
	        } else {
	            $product = new Product();
	            $product->id = $id;
	            $product->name = set_value('name');
	            $product->description = set_value('description');
	            $product->price = set_value('price');
	            $data['product'] = $product;
	            $this->load->view('product/editForm.php', $data);
	        }
        } else {
        	redirect('login', 'refresh');
        }
    }

    function delete($id)
    {
    	if ($this->session->userdata('logged_in')){    		 
	        if ($this->session->userdata('logged_in')=='admin') {
	            $this->load->model('product_model');
	
	            if (isset($id))
	                $this->product_model->delete($id);
	
	            //Then we redirect to the index page again
	            redirect('store/index', 'refresh');
	        }
	        else{
	            echo "<script>alert('BAD BOY! You are not admin!');</script>";
	            show_error("403: Read access error", 403);
	        }
        } else {
        	redirect('login', 'refresh');
        }
    }


    function addtoCart($id)
    {
    	if ($this->session->userdata('logged_in')){    		 
	        $cart = $this->session->userdata('cart');
	
	        //Check if id refers to valid product being added
	//        $this->load->model('product_model');
	//        $query = $this->product_model->get($id);
	//        if ($query=="") {
	//            print "<script>alert('Invalid product')</script>";
	//        }
	
	        if (!is_array($cart)) {
	            $cart = array();
	        }
	
	        $found = false;
	
	        foreach ($cart as $key => $item) {
	            if ($item[0] == $id) {
	
	                $cart[$key][1] = $item[1] + 1;
	
	                $found = true;
	                break;
	            }
	        }
	
	        if (!$found) {
	            $cart[] = array($id, 1);
	        }
	
	        $this->session->set_userdata('cart', $cart);
	        redirect('store/index', 'refresh');
        } else {
        	redirect('login', 'refresh');
        }
    }

    function removeFromCart($id)
    {
    	if ($this->session->userdata('logged_in')){
	    	$cart = $this->session->userdata('cart');
	
	        foreach ($cart as $key => $item) {
	            if ($item[0] == $id) {
	                unset($cart[$key]);
	                break;
	            }
	        }
	
	        $this->session->set_userdata('cart', $cart);
	        redirect('store/checkout', 'refresh');
        } else {
        	redirect('login', 'refresh');
        }
    }
    
    
    function editQuantity()
    {
    	if ($this->session->userdata('logged_in')){
	    	$this->load->library('form_validation');
	    	$this->form_validation->set_rules('quantity', 'Quantity', 'required|is_natural_no_zero');
	
	    	if ($this->form_validation->run() == true) {
	        
	    		$cart = $this->session->userdata('cart');
	    		
	    		foreach ($cart as $key => $item) {
	    			if ($item[0] == $this->input->get_post('id')) {
	    				$cart[$key][1] = $this->input->get_post('quantity');
	    				break;
	    			}
	    		}
	    		
	    		$this->session->set_userdata('cart', $cart);
	    		
	    		redirect('store/checkout', 'refresh');
	    	} else {
	    		redirect('store/checkout', 'refresh');
	       	}
       	} else {
       		redirect('login', 'refresh');
       	}   
    }
    
   function register(){
		$this->load->view('register.php');
   }
   
   function createUser() {
	   	$this->load->library('form_validation'); 	 
	   	
	   	$this->form_validation->set_rules('first','First Name','required');
	   	$this->form_validation->set_rules('last', 'Last Name','required');
	   	$this->form_validation->set_rules('login', 'Login', 'required|trim|max_length[16]|is_unique[customers.login]');
  		$this->form_validation->set_rules('email', 'Email','required|trim|max_length[45]|valid_email');
	   	$this->form_validation->set_rules('password', 'Password', 'required|max_length[16]|callback_check_password');
	   	 		   	   
	   	if ($this->form_validation->run() == true) {
	   		$this->load->model('user_model');
	   		$this->load->model('user');
	   		
	   		$user = new User();
	  		$user->first = $this->input->get_post('first');
	   		$user->last = $this->input->get_post('last');
	   		$user->login = $this->input->get_post('login');
	   		$user->email = $this->input->get_post('email');
	   		$user->password = $this->input->get_post('password');

	   		$this->user_model->insert($user);
   		  	
	   		//Then we redirect to the index page again
	   		redirect('login', 'refresh');
	   	}
	   	else {
	   		$this->load->view('register.php');
	   	}	
   	}
    
    function checkout()
    {		   
    	if ($this->session->userdata('logged_in')){    		
	        $cart = $this->session->userdata('cart');
	        // cannot proceed with empty cart
	        if (empty($cart) || !$cart){
	            echo "<script>alert('Please add some items to cart!');</script>";
	            redirect('store', 'refresh');
	        }
	
	        $this->load->model('product_model');
	        $products = $this->product_model->getAll();
	
	        // calculate total
	        $total = 0;
	        foreach ($cart as $key => $item) {
	            $product = $this->product_model->get($item[0]);
	            $total += $product->price * $item[1];
	        }
	        $this->session->set_userdata('total', $total);
	
	        $data['products'] = $products;
	        $data['cart'] = $cart;
	        $data['total'] = $total;
	
	        $this->load->view('checkout.php', $data);
        } else {
        	redirect('login', 'refresh');
        }
    }   
    
    function submitOrder()
    {    	
    	if ($this->session->userdata('logged_in')) {  
	        $cart = $this->session->userdata('cart');
	        $total = $this->session->userdata('total');
	        $this->load->model('product_model');
	        $products = $this->product_model->getAll();
	
	        $this->load->library('form_validation');
	        $this->form_validation->set_rules('creditcard_number','credit card number','required|numeric|exact_length[16]');
	        $this->form_validation->set_rules('creditcard_month', 'credit card month','required|less_than[13]|greater_than[0]');
	        $this->form_validation->set_rules('creditcard_year', 'credit card year', 'required|numeric|exact_length[2]');
	
	        if ($this->form_validation->run() == False) {
	            $this->form_validation->set_message('required', 'Please check your credit card information. All fields must be provided.');
	
	            $data['products'] = $products;
	            $data['cart'] = $cart;
	            $this->load->view('checkout',$data);
	
	        }
	        else{
	            $this->load->model('order');
	            $this->load->model('user_model');
	            $userLogin = $this->session->userdata('logged_in');
	            $user = $this->user_model->get($userLogin)->row()->id;
	
	            $cc_number = $this->input->get_post('creditcard_number');
	            $cc_month = ltrim($this->input->get_post('creditcard_month'), '0');
	            $cc_year = $this->input->get_post('creditcard_year');
	
	            // Must not record ANY order details if transaction fails.
	            $this->db->trans_begin();
	
	            // Check for expired cards.
	            if ($cc_year < date("y") || ($cc_year == date("y") and $cc_month < date("m"))){
	                $_SESSION['errmsg'] = "Expired card!";
	                $_SESSION['errno'] = 100;
	                $this->db->trans_rollback();
	                goto error;
	            }
	
	            $order_id = $this->order->submit($user,
	                $total,
	                $cc_number,
	                $cc_month,
	                $cc_year);
	
	            // now insert corresponding order_items
	            foreach ($cart as $key => $item) {
	                $this->order->insert_order_item($order_id, $item[0], $item[1]);
	            }
	
	            if ($this->db->trans_status() === FALSE)
	            {
	                $_SESSION['errmsg'] = $this->db->_error_message();
	                $_SESSION['errno'] = $this->db->_error_number();
	
	                $this->db->trans_rollback();
	                show_error($_SESSION["errmsg"], $_SESSION["errno"]);
	            }
	            else
	            {
	                $this->db->trans_commit();
	                error:
	                if (isset($_SESSION['errmsg'])){
	                    show_error($_SESSION["errmsg"]);
	                }
	                else {
	                    //remove all items from cart and print receipt
	                    $this->session->set_userdata('cart', array());
	
	                    $order = $this->order->get($order_id);
	                    $order_items = $this->order->get_order_items();
	                    $user = $this->user_model->getById($order->row()->customer_id);
	
	                    $data['products'] = $products;
	                    $data['order'] = $order;
	                    $data['user'] = $user->row();
	                    $data['order_items'] = $order_items;
	                    $data['order_id'] = $order_id;
	
	                    $this->load->view('receipt.php', $data);
	                }
	            }
	
	        }
        } else {
        	redirect('login', 'refresh');
        }
    }

    
    function displayOrders () {
    	if ($this->session->userdata('logged_in')) {	    		
	    	$this->load->model('order');
	    	$orders = $this->order->displayAll();
	    	
	    	$data['orders'] = $orders;
	    	
	    	$this->load->view('orders.php', $data);
    	} else {
    		redirect('login', 'refresh');
    	}
    }
    
    function deleteOrders () {
    	if ($this->session->userdata('logged_in')) {    		
	    	$this->load->model('order');
	    	$success = $this->order->deleteAll();
	
	        if (!$success){
	            echo "<script>alert('BAD BOY! You are not admin!');</script>";
	        }
	        else{
	            echo "<script>alert('Successfully deleted!');</script>";
	        }
	    	
	    	redirect('store', 'refresh');
    	} else {
    		redirect('login', 'refresh');
    	}
    }
    
    function deleteUsers () {
	   	if ($this->session->userdata('logged_in')) {    		 
	    	$this->load->model('user_model');
	
	    	$success = $this->user_model->deleteAll();
	
	        if (!$success){
	            echo "<script>alert('BAD BOY! You are not admin!');</script>";
	        }
	        else{
	            echo "<script>alert('Successfully deleted!');</script>";
	        }
	    	 
	    	redirect('store', 'refresh');
	    } else {
    		redirect('login', 'refresh');
    	}
    }

    function logout() {
   		$this->session->set_userdata('logged_in', NULL);
   		$this->session->set_userdata('cart', array());   		 
    	redirect('login', 'refresh');
    	 
    }
}

