<?php
/**
 * Created by PhpStorm.
 * User: Li
 * Date: 2014-11-16
 * Time: 11:57 PM
 */

class Email extends CI_Controller{

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
    	if ($this->session->userdata('logged_in')) {    		 
	    	$eparam = $this->session->userdata('eparam');
	
	        // set up gmail smtp
	        $config = Array(
	            'protocol' => 'smtp',
	            'smtp_host' => 'ssl://smtp.gmail.com',
	            'smtp_port' => 465,
	            'smtp_user' => 'bestestore2014@gmail.com',
	            'smtp_pass' => 'adminsashi',
	            'mailtype' => 'html',
	        );
	
	        $this->load->library('email', $config);
	
	        $this->email->initialize($config);
	        $this->email->set_crlf( "\r\n" );
	        $this->email->from('bestestore2014@gmail.com', 'BEST ESTORE FOR BASEBALL CARDS');
	        $email = $eparam[1];
	        //$email = $data['user']->email;
	        $this->email->to($email);
	        $this->email->subject('Receipt for your order at BEST ESTORE FOR BASEBALL CARDS');
	
	        // send receipt HTML as message body
	        $message = $eparam[0];
	        $this->email->message('<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">' . $message . '</html>');
	        $this->email->send();
	        redirect('store', 'refresh');
        } else {
        	redirect('login', 'refresh');
        }
    }
} 