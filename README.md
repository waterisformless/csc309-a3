A web front for a fictional electronic store that sells collectible baseball cards. Developed as part of [CSC309H1F Fall 2014 assignment 3](http://www.cs.toronto.edu/~delara/courses/csc309/) at the University of Toronto. 

# Technologies Used #
PHP, CodeIgniter, HTML, CSS, JavaScript, JQuery

# Special instructions #
Default entry point is the login page.

# Models #
Order - handles DB interaction for orders
Orderitem - class for encapsulating rows from order_item table
Order_object - class for encapsulating rows from orders table
Product - class for encapsulating rows from products table
Product_model - handles DB interaction with store products table
User - class for encapsulating rows from customers table
User_model - handles DB interaction with customers table

# Views #
Checkout - checkout (display) cart page
Login_view - login page
Orders - displays all orders (admin)
Receipt - HTML receipt for checked out order
Register - new user registration page
product > list - inventory page
        > editForm - edit product page
        > newForm - new product creation page
        > read - view product page

# Controllers #
email - sends receipt through email using Gmail SMTP
login - redirects user to login page
verify_login - validates login details
register - handles validation for registration fields
store -
        - displays products
        - handles admin controls (delete users/orders, display orders)
        - modifying products
        - modifying the cart
        - checking out (and validating credit card fields)
